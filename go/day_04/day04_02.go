package main

import (
	"bufio"
	"strconv"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	guardsMinutes, _, err := getData(s)
	if err != nil {
		return 0, err
	}

	max := 0
	minute := 0
	maxg := ""

	for g, v := range guardsMinutes {
		for m, i := range v {
			if i > max {
				max = i
				minute = m
				maxg = g
			}
		}
	}

	guard, err := strconv.Atoi(maxg[1:])
	if err != nil {
		return 0, err
	}

	return minute * guard, nil
}
