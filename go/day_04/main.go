package main

import (
	"adventofcodeutil"
	"bufio"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

var stringFuncs = []adventofcodeutil.StringFunc{}
var intFuncs = []adventofcodeutil.IntFunc{}

func main() {
	for _, f := range stringFuncs {
		res, err := adventofcodeutil.ExecuteStringFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
	for _, f := range intFuncs {
		res, err := adventofcodeutil.ExecuteIntFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
}

func getData(s *bufio.Scanner) (map[string]map[int]int, map[string]int, error) {
	log := sort.StringSlice([]string{})
	for s.Scan() {
		l := s.Text()
		if l != "" {
			log = append(log, l)
		}
	}
	log.Sort()

	guardsMinutes := map[string]map[int]int{}
	guardsTotals := map[string]int{}
	currentg := ""
	startm := 0
	for _, l := range log {
		parts := strings.Split(l, " ")
		minute, err := strconv.Atoi(parts[1][3:5])
		if err != nil {
			return nil, nil, err
		}

		if parts[2] == "Guard" {
			currentg = parts[3]
		}

		if parts[2] == "falls" {
			startm = minute
			if _, ok := guardsMinutes[currentg]; !ok {
				guardsMinutes[currentg] = map[int]int{}
			}
		} else if parts[2] == "wakes" {
			for i := startm; i < minute; i++ {
				guardsMinutes[currentg][i]++
				guardsTotals[currentg]++
			}
		}
	}

	return guardsMinutes, guardsTotals, nil
}
