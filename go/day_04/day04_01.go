package main

import (
	"bufio"
	"strconv"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	guardsMinutes, guardsTotals, err := getData(s)
	if err != nil {
		return 0, err
	}

	maxGuard := ""
	maxTotal := 0
	for g, m := range guardsTotals {
		if m > maxTotal {
			maxGuard = g
			maxTotal = m
		}
	}

	max := 0
	minute := 0

	v := guardsMinutes[maxGuard]
	for m, i := range v {
		if i > max {
			max = i
			minute = m
		}
	}

	guard, err := strconv.Atoi(maxGuard[1:])
	if err != nil {
		return 0, err
	}

	return minute * guard, nil
}
