package main

import (
	"bufio"
)

func doOne(s *bufio.Scanner) (int, error) {
	grid := map[string]bool{}
	overlap := map[string]bool{}
	for s.Scan() {
		l := s.Text()
		if l != "" {
			sec, err := newSection(l)
			if err != nil {
				return 0, err
			}

			region := sec.getRegion()
			for _, c := range region {
				if grid[c] {
					overlap[c] = true
				} else {
					grid[c] = true
				}
			}
		}
	}

	total := 0
	for _, b := range overlap {
		if b {
			total++
		}
	}

	return total, nil
}
