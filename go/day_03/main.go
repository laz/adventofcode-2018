package main

import (
	"adventofcodeutil"
	"fmt"
	"regexp"
	"strconv"
)

func main() {
	one, err := adventofcodeutil.ExecuteIntFunc(doOne)
	if err != nil {
		panic(err)
	}
	fmt.Println(one)

	two, err := adventofcodeutil.ExecuteStringFunc(doTwo)
	if err != nil {
		panic(err)
	}
	fmt.Println(two)
}

var sectionParser = regexp.MustCompile(`#(\d+) @ (\d+),(\d+): (\d+)x(\d+)`)

type section struct {
	id, x, y, w, h uint
}

func newSection(def string) (section, error) {
	sec := section{}
	match := sectionParser.FindStringSubmatch(def)
	if len(match) < 5 {
		return sec, fmt.Errorf("malformed section definition: %s", def)
	}
	id, err := strconv.Atoi(match[1])
	if err != nil {
		return sec, err
	}
	x, err := strconv.Atoi(match[2])
	if err != nil {
		return sec, err
	}
	y, err := strconv.Atoi(match[3])
	if err != nil {
		return sec, err
	}
	w, err := strconv.Atoi(match[4])
	if err != nil {
		return sec, err
	}
	h, err := strconv.Atoi(match[5])
	if err != nil {
		return sec, err
	}

	return section{id: uint(id), x: uint(x), y: uint(y), w: uint(w), h: uint(h)}, nil
}

func (s *section) getRegion() []string {
	res := []string{}
	for h := uint(0); h < s.h; h++ {
		for w := uint(0); w < s.w; w++ {
			res = append(res, fmt.Sprintf("%d:%d", s.x+w, s.y+h))
		}
	}
	return res
}
