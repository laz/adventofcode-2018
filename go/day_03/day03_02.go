package main

import (
	"bufio"
	"fmt"
	"strconv"
)

func doTwo(s *bufio.Scanner) (string, error) {
	id := ""
	ids := map[uint]bool{}
	grid := map[string]uint{}
	overlap := map[string]bool{}
	for s.Scan() {
		l := s.Text()
		if l != "" {
			sec, err := newSection(l)
			if err != nil {
				return "", err
			}

			ids[sec.id] = true
			region := sec.getRegion()
			for _, c := range region {
				if prev, ok := grid[c]; ok {
					delete(ids, sec.id)
					delete(ids, prev)
					overlap[c] = true
				} else {
					grid[c] = sec.id
				}
			}
		}
	}

	if len(ids) > 1 {
		return "", fmt.Errorf("got more than one result: %v", ids)
	}

	for k := range ids {
		id = strconv.Itoa(int(k))
	}

	return id, nil
}
