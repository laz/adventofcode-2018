package main

import (
	"adventofcodeutil"
	"fmt"
	"unicode"
)

var stringFuncs = []adventofcodeutil.StringFunc{}
var intFuncs = []adventofcodeutil.IntFunc{}

func main() {
	for _, f := range stringFuncs {
		res, err := adventofcodeutil.ExecuteStringFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
	for _, f := range intFuncs {
		res, err := adventofcodeutil.ExecuteIntFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
}

func collapse(in []byte) (int, error) {
	cur := 0
	size := len(in)
	for cur < size-1 {
		idxCur := cur
		idxNext := cur + 1
		left := rune(in[idxCur])
		right := rune(in[idxNext])
		cur++

		for left != right && unicode.ToLower(left) == unicode.ToLower(right) {
			in[idxCur] = 0
			in[idxNext] = 0
			idxCur--
			for idxCur > 0 && in[idxCur] == 0 {
				idxCur--
			}
			idxNext++
			if idxCur < 0 || idxNext >= size {
				break
			}
			left = rune(in[idxCur])
			right = rune(in[idxNext])
		}
		cur = idxNext
	}

	result := 0
	for _, b := range in {
		if b != 0 {
			result++
		}
	}
	return result, nil
}
