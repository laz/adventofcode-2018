package main

import (
	"bufio"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	s.Scan()
	return collapse(s.Bytes())
}
