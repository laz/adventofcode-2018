package main

import (
	"bufio"
	"fmt"
	"regexp"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

var units = "abcdefghijklmnopqrstuvwxyz"

func doTwo(s *bufio.Scanner) (int, error) {
	s.Scan()
	in := s.Bytes()
	min := len(in)

	for _, b := range units {
		regexp := regexp.MustCompile(fmt.Sprintf("[%s%s]", string(b), strings.ToUpper(string(b))))
		t, err := collapse(regexp.ReplaceAllLiteral(in, []byte{}))
		if err != nil {
			return 0, nil
		}
		if t < min {
			min = t
		}
	}

	return min, nil
}
