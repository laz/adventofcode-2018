package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	input := `dabAcCaCBAcCcaDA`
	expect := 10
	actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(input)))

	if err != nil {
		t.Errorf("unexpected error on input %s", err)
	}
	if actual != expect {
		t.Errorf("failed got %d, expected %d", actual, expect)
	}
}
