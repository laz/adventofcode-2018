package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	input := `dabAcCaCBAcCcaDA`
	expect := 4
	actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(input)))

	if err != nil {
		t.Errorf("unexpected error on input %s", err)
	}
	if actual != expect {
		t.Errorf("failed got %d, expected %d", actual, expect)
	}
}
