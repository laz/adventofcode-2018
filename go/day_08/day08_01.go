package main

import (
	"bufio"
	"strconv"

	"github.com/golang-collections/collections/stack"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	s.Split(bufio.ScanWords)
	total := 0
	tokens := stack.New()

	tokens.Push(header)
	for i := tokens.Pop(); i != nil; i = tokens.Pop() {
		cur := i.(tokenType)

		switch cur {
		case header:
			childCount := 0
			if s.Scan() {
				c, err := strconv.Atoi(s.Text())
				if err != nil {
					return 0, err
				}
				childCount = c
			}
			metadataCount := 0
			if s.Scan() {
				m, err := strconv.Atoi(s.Text())
				if err != nil {
					return 0, err
				}
				metadataCount = m
			}

			for i := 0; i < metadataCount; i++ {
				tokens.Push(metadata)
			}
			for i := 0; i < childCount; i++ {
				tokens.Push(header)
			}

		case metadata:
			if s.Scan() {
				metadataValue, err := strconv.Atoi(s.Text())
				if err != nil {
					return 0, err
				}
				total += metadataValue
			}
		}
	}

	return total, nil
}
