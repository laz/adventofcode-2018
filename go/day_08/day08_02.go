package main

import (
	"bufio"
	"strconv"

	"github.com/golang-collections/collections/stack"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

type node struct {
	metadata []int
	children []*node
	v        int
}

func (n *node) value() int {
	if n.v == 0 {
		c := len(n.children)
		if c == 0 {
			for _, i := range n.metadata {
				n.v += i
			}
		} else {
			for _, i := range n.metadata {
				if i > 0 && i <= c {
					n.v += n.children[i-1].value()
				}
			}
		}
	}
	return n.v
}

func (n *node) addMetadata(m int) {
	n.metadata = append(n.metadata, m)
}

func (n *node) addChild(c *node) {
	n.children = append([]*node{c}, n.children...)
}

type tokenPair struct {
	t tokenType
	n *node
}

func doTwo(s *bufio.Scanner) (int, error) {
	s.Split(bufio.ScanWords)
	tokens := stack.New()

	root := &node{}
	tokens.Push(tokenPair{header, root})
	for i := tokens.Pop(); i != nil; i = tokens.Pop() {
		cur := i.(tokenPair)
		tok := cur.t

		switch tok {
		case header:
			childCount := 0
			if s.Scan() {
				c, err := strconv.Atoi(s.Text())
				if err != nil {
					return 0, err
				}
				childCount = c
			}
			metadataCount := 0
			if s.Scan() {
				m, err := strconv.Atoi(s.Text())
				if err != nil {
					return 0, err
				}
				metadataCount = m
			}

			for i := 0; i < metadataCount; i++ {
				tokens.Push(tokenPair{metadata, cur.n})
			}
			if metadataCount > 0 {
				tokens.Push(tokenPair{metadataEnd, cur.n})
			}
			for i := 0; i < childCount; i++ {
				c := &node{}
				tokens.Push(tokenPair{header, c})
				cur.n.addChild(c)
			}

		case metadata:
			if s.Scan() {
				metadataValue, err := strconv.Atoi(s.Text())
				if err != nil {
					return 0, err
				}
				cur.n.addMetadata(metadataValue)
			}
		case metadataEnd:
			cur.n.value()
		}
	}

	return root.value(), nil
}
