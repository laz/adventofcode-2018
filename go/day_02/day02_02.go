package main

import (
	"bufio"
	"fmt"
)

func doTwo(s *bufio.Scanner) (string, error) {
	val := ""
	seen := map[string]bool{}

	for s.Scan() {
		t := s.Text()
		if t != "" {
			l := len(t)
			for i := 0; i < l; i++ {
				pre := string(t[0:i])
				suf := string(t[i+1 : l])
				cur := fmt.Sprintf("%s%s:%d", pre, suf, i)
				if seen[cur] {
					val = pre + suf
					break
				}
				seen[cur] = true
			}
		}
	}

	return val, nil
}
