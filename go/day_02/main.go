package main

import (
	"adventofcodeutil"
	"fmt"
)

func main() {
	result, err := adventofcodeutil.ExecuteStringFunc(doOne)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)

	result, err = adventofcodeutil.ExecuteStringFunc(doTwo)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}
