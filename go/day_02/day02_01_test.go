package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	input := `
abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab
`
	expect := "12"
	actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(input)))

	if err != nil {
		t.Errorf("unexpected error on input %s", err)
	}
	if actual != expect {
		t.Errorf("failed got %s, expected %s", actual, expect)
	}
}
