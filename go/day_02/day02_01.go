package main

import (
	"bufio"
	"strconv"
	"unicode"
)

func doOne(s *bufio.Scanner) (string, error) {
	twos, threes := 0, 0
	for s.Scan() {
		counts := map[rune]int{}
		l := s.Text()
		if l != "" {
			for _, r := range l {
				if unicode.IsLetter(r) {
					counts[r]++
				}
			}
		}
		two, three := 0, 0
		for _, v := range counts {
			if v == 2 {
				two = 1
			} else if v == 3 {
				three = 1
			}
		}

		twos += two
		threes += three
	}
	return strconv.Itoa(twos * threes), nil
}
