package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	input := `
abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz
`
	expect := "fgij"
	actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(input)))

	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}
	if actual != expect {
		t.Errorf("failed: got %s, expected %s", actual, expect)
	}
}
