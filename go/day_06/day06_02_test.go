package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	input := `
1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
`
	expect := 16
	actual, err := doTwoArg(bufio.NewScanner(bytes.NewBufferString(input)), 32)

	if err != nil {
		t.Errorf("unexpected error on input %s", err)
	}
	if actual != expect {
		t.Errorf("failed got %d, expected %d", actual, expect)
	}
}
