package main

import (
	"bufio"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	return doTwoArg(s, 10000)
}

func doTwoArg(s *bufio.Scanner, t int) (int, error) {
	points, max, err := getPoints(s)

	if err != nil {
		return 0, err
	}

	tot := 0
	for y := 0; y < max.y+1; y++ {
		for x := 0; x < max.x+1; x++ {
			distances := 0

			for _, p := range points {
				distances += distance(point{x: x, y: y}, p)
			}

			if distances < t {
				tot++
			}
		}
	}

	return tot, nil
}
