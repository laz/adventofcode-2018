package main

import (
	"adventofcodeutil"
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

var stringFuncs = []adventofcodeutil.StringFunc{}
var intFuncs = []adventofcodeutil.IntFunc{}

func main() {
	for _, f := range stringFuncs {
		res, err := adventofcodeutil.ExecuteStringFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
	for _, f := range intFuncs {
		res, err := adventofcodeutil.ExecuteIntFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
}

type point struct {
	id, x, y int
}

func distance(a, b point) int {
	dx := a.x - b.x
	if dx < 0 {
		dx = -dx
	}

	dy := a.y - b.y
	if dy < 0 {
		dy = -dy
	}

	return dx + dy
}

func printGrid(grid [][]int) {
	for _, l := range grid {
		fmt.Println(l)
	}
}

func getPoints(s *bufio.Scanner) (map[int]point, point, error) {
	points := map[int]point{}
	max := point{}

	for i := 1; s.Scan(); i++ {
		l := s.Text()
		if strings.Index(l, ", ") > -1 {
			parts := strings.Split(l, ", ")

			x, err := strconv.Atoi(parts[0])
			if err != nil {
				return nil, point{}, err
			}

			y, err := strconv.Atoi(parts[1])
			if err != nil {
				return nil, point{}, err
			}

			p := point{i, x, y}
			points[p.id] = p
			if p.x > max.x {
				max.x = p.x
			}
			if p.y > max.y {
				max.y = p.y
			}
		}
	}

	return points, max, nil
}
