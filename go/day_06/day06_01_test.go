package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	input := `
1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
`
	expect := 17
	actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(input)))

	if err != nil {
		t.Errorf("unexpected error on input %s", err)
	}
	if actual != expect {
		t.Errorf("failed got %d, expected %d", actual, expect)
	}
}
