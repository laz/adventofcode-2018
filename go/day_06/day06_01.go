package main

import (
	"bufio"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	points, max, err := getPoints(s)

	if err != nil {
		return 0, err
	}

	pointTotals := map[int]int{}
	infinitePoints := map[int]bool{}

	for y := 0; y < max.y+1; y++ {
		for x := 0; x < max.x+1; x++ {
			minID := 0
			min := max.y + max.x + 2
			dup := min + 1
			for _, p := range points {
				d := distance(point{x: x, y: y}, p)
				if d == min {
					dup = min
				} else if d < min {
					min = d
					minID = p.id
				}
			}
			if dup != min && !infinitePoints[minID] {
				if y != 0 && y != max.y && x != 0 && x != max.x {
					pointTotals[minID]++
				} else {
					infinitePoints[minID] = true
					pointTotals[minID] = 0
				}
			}
		}
	}

	ans := 0
	for _, i := range pointTotals {
		if i > ans {
			ans = i
		}
	}

	return ans, nil
}

func occurs(p int, grid [][]int) int {
	tot := 0
	my := len(grid) - 1
	for y, r := range grid {
		mx := len(r) - 1
		for x := range r {
			if grid[y][x] == p {
				if y == 0 || y == my || x == 0 || x == mx {
					return 0
				}
				tot++
			}
		}
	}
	return tot
}
