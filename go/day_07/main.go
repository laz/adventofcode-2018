package main

import (
	"adventofcodeutil"
	"fmt"
)

var stringFuncs = []adventofcodeutil.StringFunc{}
var intFuncs = []adventofcodeutil.IntFunc{}

func main() {
	for _, f := range stringFuncs {
		res, err := adventofcodeutil.ExecuteStringFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
	for _, f := range intFuncs {
		res, err := adventofcodeutil.ExecuteIntFunc(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
}
