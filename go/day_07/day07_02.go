package main

import (
	"bufio"
	"sort"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

type worker struct {
	job, work int
}

func doTwo(s *bufio.Scanner) (int, error) {
	return doTwoArg(s, 5, 60)
}

func doTwoArg(s *bufio.Scanner, w, o int) (int, error) {
	depends := map[int]map[int]bool{}
	prereqs := map[int]map[int]bool{}

	for s.Scan() {
		l := s.Text()
		if len(l) > 35 {
			p := int(l[5])
			c := int(l[36])

			if _, ok := depends[c]; !ok {
				depends[c] = map[int]bool{}
			}
			depends[c][p] = true

			if _, ok := prereqs[p]; !ok {
				prereqs[p] = map[int]bool{}
			}
			prereqs[p][c] = true
		}
	}

	candidates := sort.IntSlice{}

	for b := range prereqs {
		if _, ok := depends[b]; !ok {
			candidates = append(candidates, b)
		}
	}
	candidates.Sort()

	total := 0
	workers := make([]worker, w)
	done := false

	for !done {

		free := []int{}
		d := 0

		for i, w := range workers {
			c := w.work
			if c > 0 {
				c--
				if c == 0 {
					d = w.job
				}
			}
			if c == 0 {
				free = append(free, i)
			}
			workers[i].work = c
		}

		if d > 0 {
			next := prereqs[d]
			delete(prereqs, d)
			for k := range next {
				v, ok := depends[k]
				if ok {
					delete(v, d)
					if len(v) == 0 {
						delete(depends, k)
					}
				}
				if !ok || len(v) == 0 {
					candidates = append(candidates, k)
				}
			}
			candidates.Sort()
		}

		for _, f := range free {
			if len(candidates) > 0 {
				c := candidates[0]
				workers[f] = worker{c, o + c - 64}

				if len(candidates) > 1 {
					candidates = candidates[1:]
				} else {
					candidates = sort.IntSlice{}
				}
			}
		}

		if len(depends) == 0 {
			idle := 0
			for _, w := range workers {
				if w.work == 0 {
					idle++
				}
			}
			done = idle == len(workers)
		}

		if !done {
			total++
		}
	}

	return total, nil
}
