package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	input := `
Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
`
	expect := "CABDFE"
	actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(input)))

	if err != nil {
		t.Errorf("unexpected error on input %s", err)
	}
	if actual != expect {
		t.Errorf("failed got %s, expected %s", actual, expect)
	}
}
