package main

import (
	"bufio"
	"sort"
)

func init() {
	stringFuncs = append(stringFuncs, doOne)
}

func doOne(s *bufio.Scanner) (string, error) {
	depends := map[int]map[int]bool{}
	prereqs := map[int]map[int]bool{}

	for s.Scan() {
		l := s.Text()
		if len(l) > 35 {
			p := int(l[5])
			c := int(l[36])

			if _, ok := depends[c]; !ok {
				depends[c] = map[int]bool{}
			}
			depends[c][p] = true

			if _, ok := prereqs[p]; !ok {
				prereqs[p] = map[int]bool{}
			}
			prereqs[p][c] = true
		}
	}

	ans := []byte{}
	candidates := sort.IntSlice{}

	for b := range prereqs {
		if _, ok := depends[b]; !ok {
			candidates = append(candidates, b)
		}
	}
	candidates.Sort()

	done := false
	for !done {
		c := candidates[0]
		ans = append(ans, byte(c))

		if len(candidates) > 1 {
			candidates = candidates[1:]
		} else {
			candidates = sort.IntSlice{}
		}
		next := prereqs[c]
		delete(prereqs, c)
		for k := range next {
			v, ok := depends[k]
			if ok {
				delete(v, c)
				if len(v) == 0 {
					delete(depends, k)
				}
			}
			if !ok || len(v) == 0 {
				candidates = append(candidates, k)
			}
		}
		done = len(depends) == 0
		candidates.Sort()
	}

	return string(ans), nil
}
