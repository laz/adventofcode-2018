package main

import (
	"bufio"
	"strconv"
)

func doOne(s *bufio.Scanner) (int, error) {
	total := 0
	for s.Scan() {
		l := s.Text()
		if l != "" {
			i, err := strconv.Atoi(s.Text())
			if err != nil {
				return 0, err
			}
			total += i
		}
	}
	return total, nil
}
