package main

import (
	"bufio"
	"strconv"
)

func doTwo(s *bufio.Scanner) (int, error) {
	val := 0
	total := 0
	vals := []int{}
	seen := map[int]bool{0: true}

	for s.Scan() {
		l := s.Text()
		if l != "" {
			i, err := strconv.Atoi(s.Text())
			if err != nil {
				return 0, err
			}

			total += i
			if !seen[total] {
				seen[total] = true
			} else {
				vals = []int{}
				val = total
				break
			}

			vals = append(vals, i)
		}
	}

	found := len(vals) == 0
	for !found {
		for _, i := range vals {
			total += i
			if seen[total] {
				val = total
				found = true
				break
			}
		}
	}

	return val, nil
}
