package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	tests := map[string]int{
		"+1\n-2\n+3\n+1\n": 3,
		"+1\n+1\n+1":       3,
		"+1\n+1\n-2":       0,
		"-1\n-2\n-3":       -6,
	}

	for test, expect := range tests {
		actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
