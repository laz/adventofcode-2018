package main

import (
	"adventofcodeutil"
	"fmt"
)

func main() {
	result, err := adventofcodeutil.ExecuteIntFunc(doOne)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)

	result, err = adventofcodeutil.ExecuteIntFunc(doTwo)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}
