package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	tests := map[string]int{
		"+1\n-1":             0,
		"+3\n+3\n+4\n-2\n-4": 10,
		"-6\n+3\n+8\n+5\n-6": 5,
		"+7\n+7\n-2\n-7\n-4": 14,
	}

	for test, expect := range tests {
		actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
