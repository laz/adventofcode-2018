package main

import (
	"bufio"
	"strconv"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	s.Scan()
	l := strings.Split(s.Text(), " ")

	numPlayers, err := strconv.Atoi(l[0])
	if err != nil {
		return 0, nil
	}

	totalMarbles, err := strconv.Atoi(l[6])
	if err != nil {
		return 0, nil
	}

	board := make([]int, totalMarbles)
	board[1] = 1
	scores := make([]int, numPlayers)

	current := 1
	size := 2
	for turn := 2; turn <= totalMarbles; turn++ {
		if turn%23 != 0 {
			current += 2
			if current == size+1 {
				current = 1
			}
			size++
			for i := size - 1; i > current; i-- {
				board[i] = board[i-1]
			}
			board[current] = turn
		} else {
			player := turn % numPlayers
			scores[player] += turn
			current -= 7
			if current < 0 {
				current += size
			}
			scores[player] += board[current]

			for i := current; i < size; i++ {
				board[i] = board[i+1]
			}
			size--
		}
	}

	high := 0
	for _, s := range scores {
		if s > high {
			high = s
		}
	}
	return high, nil
}
