package main

import (
	"bufio"
	"container/list"
	"strconv"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	s.Scan()
	l := strings.Split(s.Text(), " ")

	numPlayers, err := strconv.Atoi(l[0])
	if err != nil {
		return 0, nil
	}

	totalMarbles, err := strconv.Atoi(l[6])
	if err != nil {
		return 0, nil
	}

	totalMarbles *= 100
	board := list.New()
	current := board.PushFront(0)
	scores := make([]int, numPlayers)

	for turn := 1; turn <= totalMarbles; turn++ {
		if turn%23 != 0 {
			current = current.Next()
			if current == nil {
				current = board.Front()
			}
			board.InsertAfter(turn, current)
			current = current.Next()
		} else {
			player := turn % numPlayers
			scores[player] += turn

			for i := 0; i < 7; i++ {
				current = current.Prev()
				if current == nil {
					current = board.Back()
				}
			}
			scores[player] += current.Value.(int)
			next := current.Next()
			board.Remove(current)
			current = next
		}
	}

	high := 0
	for _, s := range scores {
		if s > high {
			high = s
		}
	}
	return high, nil
}
