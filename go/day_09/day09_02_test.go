package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	inputs := []string{
		"9 players; last marble is worth 25 points",
		"10 players; last marble is worth 1618 points",
		"13 players; last marble is worth 7999 points",
		"17 players; last marble is worth 1104 points",
		"21 players; last marble is worth 6111 points",
		"30 players; last marble is worth 5807 points",
	}
	expects := []int{
		22563,
		74765078,
		1406506154,
		20548882,
		507583214,
		320997431,
	}

	for i, input := range inputs {
		expect := expects[i]
		actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(input)))

		if err != nil {
			t.Errorf("unexpected error on input %s", err)
		}
		if actual != expect {
			t.Errorf("failed got %d, expected %d", actual, expect)
		}
	}
}
